## [1.1.1](https://gitlab.com/to-be-continuous/sbt/compare/1.1.0...1.1.1) (2023-01-27)


### Bug Fixes

* "Add registry name in all Docker images" ([663c9c6](https://gitlab.com/to-be-continuous/sbt/commit/663c9c688ddd0862938510e7a5328afdc346b964))

# [1.1.0](https://gitlab.com/to-be-continuous/sbt/compare/1.0.0...1.1.0) (2022-12-19)


### Features

* add a job generating software bill of materials ([efcaf0f](https://gitlab.com/to-be-continuous/sbt/commit/efcaf0fd136248d686f6964a20cf98d734241399))

# 1.0.0 (2022-08-21)


### Features

* initial template version ([926ba1d](https://gitlab.com/to-be-continuous/sbt/commit/926ba1d0100a1a6dc4162ef56d7573267fb05a53))
